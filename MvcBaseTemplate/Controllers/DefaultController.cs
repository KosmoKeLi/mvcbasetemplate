﻿using System.Web.Mvc;

namespace MvcBaseTemplate.Controllers
{
    public class DefaultController : Controller
    {
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }
    }
}