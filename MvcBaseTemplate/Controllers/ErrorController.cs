﻿using System.Web.Mvc;

namespace MvcBaseTemplate.Controllers
{
    public class ErrorController : Controller
    {
        [Route("error/401")]
        public ActionResult Erro401()
        {
            Response.StatusCode = 401;

            return Content("Error 401");
        }

        [Route("error/403")]
        public ActionResult Erro403()
        {
            Response.StatusCode = 403;

            return Content("Error 403");
        }

        [Route("error/404")]
        public ActionResult Erro404()
        {
            Response.StatusCode = 404;

            return Content("Error 404");
        }
    }
}