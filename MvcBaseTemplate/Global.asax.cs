﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MvcBaseTemplate.App_Start;

namespace MvcBaseTemplate
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterFilters(GlobalFilters.Filters);
        }
    }
}
