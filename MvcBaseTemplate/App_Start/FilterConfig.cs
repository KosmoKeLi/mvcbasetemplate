﻿using System.Web.Mvc;

namespace MvcBaseTemplate.App_Start
{
    public static class FilterConfig
    {
        public static void RegisterFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}